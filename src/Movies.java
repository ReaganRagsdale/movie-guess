import java.util.Scanner;

public class Movies {
	Scanner input = new Scanner(System.in);
	String[] movies = { "avatar", "inception", "avengers" };
	int d = 0;
	String movie = movies[d];
	char[] displayMovie = new char[movie.length()];
	int g = 0;
	String reply;
	int count = 0;

	public void printMovie() {

		displayMovie = new char[movie.length()];
		for (int l = 0; l < movie.length(); l++) {

			displayMovie[l] = '-';

		}
		System.out.println(displayMovie);

		System.out.println(" ");
	}

	public char userGuess() {

		System.out.println("What is your guess?");
		String guess1 = input.next();
		char guess = guess1.charAt(0);

		if (Character.isDigit(guess)) {
			System.out.println("That is not a letter. Please enter a letter.");
			return userGuess();
		}
		if (guess1.length() > 1) {
			System.out.println("Please only enter one letter");
			return userGuess();

		}
		return guess;
	}

	public boolean displayGuess(char guess) {
		boolean found = false;
		
		for (int n = 0; n < movie.length(); n++) {
			if (guess == movie.charAt(n)) {
				found = true;
				
				displayMovie[n] = guess;

			}
		}

		if (!found) {
			g++;
			System.out.println("Guess incorrect. " + (6 - g) + " lives left");
			// found = false;
			// userGuess();
		}
		System.out.println(displayMovie);
		System.out.print(" " + (6 - g) + " lives left");
		return found;
		// if (found)
		// System.out.println("Here3");
	}

	public void printMessageAndMovie(String message1) {

		if (d == 2) {
			System.out.println(message1);
			System.out.println("Congratulations you won the game!");
			System.exit(0);

		} else {
			System.out.println(message1);
			System.out.print("Here is your next movie to guess");
			System.out.println(" ");
			d++;
			if (d < 3) {
				this.movie = movies[d];
				printMovie();
			}
			g = 0;

		}
	}

	public boolean readyToGuess() {

		System.out.println(" ");
		System.out.println("Are you ready to answer? yes or no");
		reply = input.next();
		if (!reply.equals("yes") && (!reply.equals("no"))) {
			System.out.println("Please enter yes or no");
			readyToGuess();
		}
		return reply.equals("yes");

	}

	public boolean goForGuess() {

		System.out.println("What is the answer?");
		String answer = input.next();
		if (answer.equals(movie)) {
			return movie.equals(answer);
		} else if (!answer.equals(movie)) {
			System.out.println("Incorrect answer, please guess another letter");

		}
		return false;
	}

	public void noForGuess() {
		if (reply.equals("no")) {
			g++;
		}
	}

}