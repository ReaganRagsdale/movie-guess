import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int g = 0;

		System.out.println("Here is your first movie to guess. Enter a letter to start guessing.");
		Movies movies = new Movies();
		movies.printMovie();
		for (g = 0; g < 1000; g++) {
			char guess = movies.userGuess();
			boolean guessFound = movies.displayGuess(guess);
			if (guessFound) {
				if (g == 6) {
					movies.printMessageAndMovie("You ran out of guesses");
					g = 0;
				} else if (movies.readyToGuess()) {
					if (movies.goForGuess()) {
						movies.printMessageAndMovie("Correct!");
						

					}
				} else {
					movies.noForGuess();
				}
			}


		}

	}

}
